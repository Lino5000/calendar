use eframe::{epi, egui};
use chrono::offset::Local;

mod date;
use date::*;

#[derive(Default)]
struct CalendarApp {
    focus_date: Date
}

impl epi::App for CalendarApp {
    fn name(&self) -> &str {
        "Calendar"
    }

    fn update(&mut self, ctx: &egui::CtxRef, _frame: &mut epi::Frame<'_>) {
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.heading(Local::now().format("%c").to_string());
            ui.horizontal(|ui| {
                ui.heading(format!("{}", self.focus_date));
                if ui.button("<").clicked() {
                    self.focus_date -= Duration { months: 1, ..Default::default() };
                }
                if ui.button(">").clicked() {
                    self.focus_date += Duration { months: 1, ..Default::default() };
                }
            });
        });
    }
}

fn main() {
    let app = CalendarApp::default();
    let native_options = eframe::NativeOptions::default();
    eframe::run_native(Box::new(app), native_options);
}
