use std::{fmt, default, ops};
use chrono::{offset::Local, Datelike};

#[derive(Clone, Copy, Debug)]
pub struct Date {
    pub year: i32,
    pub month: i32,
    pub day: i32
}

impl fmt::Display for Date {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:04}-{:02}-{:02}", self.year, self.month+1, self.day+1)
    }
}

impl default::Default for Date {
    fn default() -> Self {
        Self::today()
    }
}

impl ops::Add<Duration> for Date {
    type Output = Self;
    fn add(self, rhs: Duration) -> Self::Output {
        Self {
            day: (self.day as i32) + rhs.days,
            month: (self.month as i32) + rhs.months,
            year: self.year + (rhs.years as i32),
        }.normalised()
    }
}

impl ops::AddAssign<Duration> for Date {
    fn add_assign(&mut self, rhs: Duration) {
        *self = *self + rhs;
    }
}

impl ops::Sub<Duration> for Date {
    type Output = Self;
    fn sub(self, rhs: Duration) -> Self::Output {
        Self {
            day: self.day - rhs.days,
            month: self.month - rhs.months,
            year: self.year - rhs.years,
        }.normalised()
    }
}

impl ops::SubAssign<Duration> for Date {
    fn sub_assign(&mut self, rhs: Duration) {
        *self = *self - rhs;
    }
}

impl Date {
    pub fn today() -> Self {
        let today = Local::today();
        Self {
            year: today.year(),
            month: today.month0() as i32,
            day: today.day0() as i32
        }
    }

    pub fn normalised(&self) -> Self {
        if self.is_normalised() {
            *self
        } else {
            let mut out = Self {
                day: self.day,
                month: self.month,
                year: self.year
            };

            out.year += out.month.div_euclid(12);
            out.month = out.month.rem_euclid(12);

            while out.day < 0 {
                if out.month == 0 {
                    out.year -= 1
                }
                out.month = (out.month - 1).rem_euclid(12);
                out.day += out.days_in_month();
            }

            while out.day >= out.days_in_month() {
                if out.month == 11 {
                    out.year += 1
                }
                out.day -= out.days_in_month();
                out.month = (out.month + 1).rem_euclid(12);
            }

            out
        }
    }

    pub fn is_normalised(&self) -> bool {
        self.month >= 0 && self.month < 12 && self.day >= 0
            && self.day < self.days_in_month()
    }

    pub fn is_leap_year(&self) -> bool {
       self.year.rem_euclid(400) == 0
           || (self.year.rem_euclid(4) == 0 && self.year.rem_euclid(100) != 0)
    }

    pub fn days_in_month(&self) -> i32 {
        match self.month {
            1 => if self.is_leap_year() { 29 } else { 28 },
            0..=6 => if self.month % 2 == 0 { 31 } else { 30 },
            7..=11 => if self.month % 2 == 0 { 30 } else { 31 },
            _ => panic!("Invalid month `{}`; should have 0 <= month <= 11", self.month),
        }
    }
}

#[derive(Default, Clone, Copy, Debug)]
pub struct Duration {
    pub years: i32,
    pub months: i32,
    pub days: i32,
    // TODO: Add support for sub-day durations
}

impl fmt::Display for Duration {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}{}{}",
            if self.years > 0 {
                format!("{} years ", self.years)
            } else {
                format!("")
            },
            if self.months > 0 {
                format!("{} months ", self.months)
            } else {
                format!("")
            },
            if self.days > 0 {
                format!("{} days ", self.days)
            } else {
                format!("")
            })
    }
}
