# Calendar

This is a holiday project to create a basic Calendar app, based on the prompt:

> a calendar that expands a date when you click on it and displays all the
> text, but when you unselect it, it only displays the title

Currently it is a GUI app using [egui](https://github.com/emilk/egui), but I
may use it as a learning exercise for TUI apps in the future.
